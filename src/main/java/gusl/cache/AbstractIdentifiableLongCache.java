package gusl.cache;

import gusl.core.eventbus.OnEvent;
import gusl.core.utils.Utils;
import gusl.model.Identifiable;
import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheAction;
import gusl.model.cache.CacheActionEvent;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

/**
 * @param <DO>
 * @author dhudson
 */
public abstract class AbstractIdentifiableLongCache<DO extends Identifiable<Long>> extends AbstractIdDOCache<DO> {

    private final Map<Long, DO> theMap = new ConcurrentHashMap<>();

    public AbstractIdentifiableLongCache() {
    }

    public Optional<DO> getSingleWithPredicate(Predicate<DO> predicate) {
        if (predicate == null) {
            return Optional.empty();
        }

        return Utils.safeStream(theMap.values()).filter(predicate).findFirst();
    }

    @Override
    public List<DO> getWithPredicate(Predicate<DO> predicate) {
        if (predicate == null) {
            predicate = getAllPredicate();
        }

        return Utils.safeStream(theMap.values())
                .filter(predicate)
                .collect(Collectors.toList());
    }

    @Override
    public void populateCache(List<? extends DO> contents) {
        if (contents != null) {
            for (DO content : contents) {
                add(content);
            }
        }
        setPopulated();

    }

    @Override
    public DO get(Long id) {
        return theMap.get(id);
    }

    @Override
    public DO getNoFetch(Long id) {
        return theMap.get(id);
    }

    @Override
    public Optional<DO> getOpt(Long id) {
        return Optional.ofNullable(get(id));
    }

    @Override
    public void add(DO data) {
        theMap.put(data.getId(), data);
        setLastUpdateTime();
    }

    @Override
    public void update(DO data) {
        add(data);
    }

    @Override
    public boolean remove(DO data) {
        return remove(data.getId());
    }

    @Override
    public void clear() {
        theMap.clear();
        setLastUpdateTime();
    }

    public boolean remove(Long id) {
        boolean didRemove = (theMap.remove(id) != null);
        if (didRemove) {
            setLastUpdateTime();
        }
        return didRemove;
    }

    public boolean contains(Long id) {
        return theMap.containsKey(id);
    }

    public Collection<DO> values() {
        return theMap.values();
    }

    @Override
    public long getCacheSize() {
        return theMap.values().size();
    }

    @OnEvent
    public void cacheListener(CacheActionEvent event) {
        switch (event.getCacheAction()) {
            case DUMP:
                dump();
                break;
            case CACHE_SIZE:
                logCacheSize();
                break;
        }
    }

    public <T> boolean hasEvent(AbstractCacheActionEvent<T> event) {
        return nonNull(event) && nonNull(event.getCacheAction()) && (nonNull(event.getDataObject()) || CacheAction.CLEAR == event.getCacheAction());
    }


}
