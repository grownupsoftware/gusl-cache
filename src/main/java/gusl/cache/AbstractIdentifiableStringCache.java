package gusl.cache;

import gusl.core.utils.Utils;
import gusl.model.Identifiable;
import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheAction;
import lombok.CustomLog;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

/**
 * @param <DO>
 * @author dhudson
 */
@CustomLog
public abstract class AbstractIdentifiableStringCache<DO extends Identifiable<String>> extends AbstractKeyDOCache<String, DO> {

    private final Map<String, DO> theMap = new ConcurrentHashMap<>();

    public AbstractIdentifiableStringCache() {
    }

    public Optional<DO> getSingleWithPredicate(Predicate<DO> predicate) {
        if (predicate == null) {
            return Optional.empty();
        }

        return Utils.safeStream(theMap.values()).filter(predicate).findFirst();
    }

    @Override
    public List<DO> getWithPredicate(Predicate<DO> predicate) {
        if (predicate == null) {
            predicate = getAllPredicate();
        }

        return Utils.safeStream(theMap.values())
                .filter(predicate)
                .collect(Collectors.toList());
    }

    @Override
    public void populateCache(List<? extends DO> contents) {
        for (DO content : contents) {
            add(content);
        }
        setPopulated();
    }

    @Override
    public DO get(String id) {
        return theMap.get(id);
    }

    @Override
    public DO getNoFetch(String id) {
        return theMap.get(id);
    }

    @Override
    public void add(DO data) {
        theMap.put(data.getId(), data);
        setLastUpdateTime();
    }

    public void add(DO data, boolean sendEvent) {
        add(data);
    }

    @Override
    public void clear() {
        theMap.clear();
        setLastUpdateTime();
    }

    @Override
    public void update(DO data) {
        add(data);
    }

    public void update(DO data, boolean sendEvent) {
        add(data);
    }

    @Override
    public boolean remove(DO data) {
        return remove(data.getId());
    }
    public boolean remove(String id) {
        boolean didRemove = (theMap.remove(id) != null);

        if (didRemove) {
            setLastUpdateTime();
        }
        return didRemove;
    }

    public boolean contains(String id) {
        return theMap.containsKey(id);
    }

    public Collection<DO> values() {
        return theMap.values();
    }

    @Override
    public Optional<DO> getOpt(String id) {
        return Optional.ofNullable(get(id));
    }

    public <T> boolean hasEvent(AbstractCacheActionEvent<T> event) {
        return nonNull(event) && nonNull(event.getCacheAction()) && (nonNull(event.getDataObject()) || CacheAction.CLEAR == event.getCacheAction());
    }

    protected String getAdminUser() {
        return "Cache";
    }

    @Override
    public long getCacheSize() {
        return theMap.values().size();
    }

}
