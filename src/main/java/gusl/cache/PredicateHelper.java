package gusl.cache;

import com.google.common.base.CaseFormat;
import gusl.annotations.form.page.PageRequest;
import gusl.annotations.form.page.QueryCondition;
import gusl.annotations.form.page.QueryConditions;
import gusl.annotations.form.page.QueryOperand;
import gusl.core.exceptions.GUSLErrorException;
import lombok.CustomLog;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static gusl.core.utils.LambdaGUSLErrorExceptionHelper.rethrowGUSLErrorFunction;
import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@CustomLog
public class PredicateHelper {

    public static <T> List<Predicate<T>> getPredicatesFromPageRequest(final PageRequest request, Class<T> dataClass) {
        if (isNull(request)
                || isNull(request.getQueryConditions())
                || isNull(request.getQueryConditions().getConditions())
                || request.getQueryConditions().getConditions().isEmpty()
        ) {
            return new ArrayList<>(0);
        }
        return getPredicates(request.getQueryConditions(), dataClass);
    }

    public static <T> List<Predicate<T>> getPredicates(final QueryConditions queryConditions, Class<T> dataClass) {
        if (isNull(queryConditions) || isNull(queryConditions.getConditions()) || queryConditions.getConditions().isEmpty()) {
            return new ArrayList<>(0);
        }
        return safeStream(queryConditions.getConditions())
                .filter(condition -> nonNull(condition.getFieldName()))
                .filter(condition -> nonNull(condition.getValue()))
                .filter(condition -> condition.getOperand() == QueryOperand.EQUALS
                        || condition.getOperand() == QueryOperand.NOT_EQUALS)
                .map(rethrowGUSLErrorFunction(condition -> createPredicate(condition, dataClass))).collect(Collectors.toList());
    }

    private static String normaliseName(String name) {
        if (name.contains("-")) {
            return CaseFormat.LOWER_HYPHEN.to(CaseFormat.UPPER_CAMEL, name);
        } else if (name.contains("_")) {
            return CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, name);
        }
        return CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, name);
    }

    private static String getMethodName(String name) {
        return "get" + name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    private static <T> Predicate<T> createPredicate(QueryCondition queryCondition, Class<T> dataClass) throws GUSLErrorException {
        String fieldName = normaliseName(queryCondition.getFieldName());
        Method getterMethod;
        try {
            getterMethod = dataClass.getMethod(getMethodName(fieldName));
        } catch (NoSuchMethodException ex) {
            throw new GUSLErrorException(CacheErrors.FAILED_TO_GET_GETTER_METHOD.getError(dataClass.getCanonicalName(), fieldName));
        }
        return createPredicate(queryCondition, getterMethod);
    }

    public static <T> Predicate<T> createPredicate(QueryCondition queryCondition, Method getterMethod) {

        return p -> {
            try {
                switch (queryCondition.getOperand()) {
                    case EQUALS:
                        if (getterMethod.getReturnType().isEnum()) {
                            var enumValue = (Enum) getterMethod.invoke(p);
                            if (isNull(enumValue)) {
                                return false;
                            }
                            return enumValue.name().equals(queryCondition.getValue());
                        } else {
                            switch (getterMethod.getReturnType().getSimpleName()) {
                                case "String":
                                    String strValue = (String) getterMethod.invoke(p);
                                    if (queryCondition.getValue() instanceof Integer) {
                                        return (String.valueOf(queryCondition.getValue())).equals(strValue);
                                    } else {
                                        return queryCondition.getValue().equals(strValue);
                                    }
                                case "Long":
                                    Long longValue = (Long) getterMethod.invoke(p);
                                    return ((Long) queryCondition.getValue()).longValue() == longValue;
                                default:
                                    throw new GUSLErrorException(CacheErrors.TYPE_NOT_SUPPORTED.getError("Return type of [" + getterMethod.getReturnType().getSimpleName() + "] not supported"));
                            }
                        }
                    case NOT_EQUALS:
                        if (getterMethod.getReturnType().isEnum()) {
                            var enumValue = (Enum) getterMethod.invoke(p);
                            if (isNull(enumValue)) {
                                return true;
                            }
                            return !enumValue.name().equals(queryCondition.getValue());
                        } else {
                            switch (getterMethod.getReturnType().getSimpleName()) {
                                case "String":
                                    String strValue = (String) getterMethod.invoke(p);
                                    return !queryCondition.getValue().equals(strValue);
                                case "Long":
                                    Long longValue = (Long) getterMethod.invoke(p);
                                    return ((Long) queryCondition.getValue()).longValue() != longValue;
                                default:
                                    throw new GUSLErrorException(CacheErrors.TYPE_NOT_SUPPORTED.getError("Return type of [" + getterMethod.getReturnType().getSimpleName() + "] not supported"));
                            }
                        }
                    case QUERY_STRING:
                    case DATE_RANGE:
                    default:
                        throw new GUSLErrorException(CacheErrors.TYPE_NOT_SUPPORTED.getError("Query operand [" + queryCondition.getOperand().getName() + "] not supported"));
                }
            } catch (GUSLErrorException | IllegalAccessException | InvocationTargetException e) {
                logger.warn("Failed to apply predicated", e);
                return false;
            }
        };
    }

}
