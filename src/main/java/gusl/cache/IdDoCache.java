package gusl.cache;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * @param <DO>
 * @author dhudson
 */
public interface IdDoCache<DO> extends Timestampable {

    List<DO> getAll();

    List<DO> getWithPredicate(Predicate<DO> predicate);

    void populateCache(List<? extends DO> contents);

    DO get(Long id);

    DO getNoFetch(Long id);

    Optional<DO> getOpt(Long id);

    void listen();

    long getCacheSize();
}
