package gusl.cache;

import com.google.common.base.CaseFormat;
import gusl.annotations.form.page.OrderDirection;
import gusl.annotations.form.page.PageRequest;
import gusl.core.exceptions.GUSLErrorException;
import gusl.model.money.MoneyDO;
import lombok.CustomLog;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static java.util.Objects.isNull;

@CustomLog
public class ComparatorCache<T> {

    private final Map<String, Comparator<T>> theMapOfComparators = new ConcurrentHashMap<>(3);
    private Class<T> theDataClass;

    private ComparatorCache() {
    }

    public ComparatorCache(Class<T> dataClass) {
        theDataClass = dataClass;
    }

    public Comparator<T> getComparator(PageRequest request) throws GUSLErrorException {
        if (isNull(request) || isNull(request.getOrderParam())) {
            // return a non sort comparator
            return (o1, o2) -> 0;
        }
        return getComparator(request.getOrderParam(), request.getOrderDirection());
    }

    public Comparator<T> getComparator(String field, OrderDirection direction) throws GUSLErrorException {
        if (isNull(field)) {
            // return a non sort comparator
            return (o1, o2) -> 0;
        }
        Comparator<T> comparator = theMapOfComparators.get(field);
        if (isNull(comparator)) {
            comparator = createComparator(field);
        }
        switch (direction) {
            case DESC:
                return comparator.reversed();
            case ASC:
            default:
                return comparator;
        }
    }

    private static String normaliseName(String name) {
        if (name.contains("-")) {
            return CaseFormat.LOWER_HYPHEN.to(CaseFormat.UPPER_CAMEL, name);
        } else if (name.contains("_")) {
            return CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, name);
        }
        return CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, name);
    }

    private static String getMethodName(String prefix, String name) {
        return prefix + name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    private Comparator<T> createComparator(String field) throws GUSLErrorException {
        String fieldName = normaliseName(field);
        Method getterMethod;
        try {
            getterMethod = theDataClass.getMethod(getMethodName("get", fieldName));
        } catch (NoSuchMethodException ex) {
            throw new GUSLErrorException(CacheErrors.ERR_SERVER_ERROR.getError(theDataClass.getCanonicalName(), fieldName));
        }

        Comparator<T> comparator = createComparator(getterMethod);
        theMapOfComparators.put(field, comparator);

        return comparator;
    }

    private Comparator<T> createComparator(Method getterMethod) {
        return (o1, o2) -> {
            try {
                switch (getterMethod.getReturnType().getSimpleName()) {
                    case "String":
                        return compareStrings((String) getterMethod.invoke(o1), (String) getterMethod.invoke(o2));
                    case "Long":
                    case "long":
                        return compareLong((Long) getterMethod.invoke(o1), (Long) getterMethod.invoke(o2));
                    case "Integer":
                    case "int":
                        return compareInteger((Integer) getterMethod.invoke(o1), (Integer) getterMethod.invoke(o2));
                    case "Money":
                        return compareMoney((MoneyDO) getterMethod.invoke(o1), (MoneyDO) getterMethod.invoke(o2));
                    case "Date":
                        return compareDate((Date) getterMethod.invoke(o1), (Date) getterMethod.invoke(o2));

                }

                return 0;
            } catch (IllegalAccessException | InvocationTargetException e) {
                logger.warn("Failed to apply predicated", e);
                return 0;
            }
        };
    }

    private int compareStrings(String val1, String val2) {
        if (isNull(val1) && isNull(val2)) {
            return 0;
        } else if (isNull(val1)) {
            return 1;
        } else if (isNull(val2)) {
            return -1;
        } else {
            return val1.compareTo(val2);
        }
    }

    private int compareLong(Long val1, Long val2) {
        if (isNull(val1) && isNull(val2)) {
            return 0;
        } else if (isNull(val1)) {
            return 1;
        } else if (isNull(val2)) {
            return -1;
        } else {
            return val1.compareTo(val2);
        }
    }

    private int compareInteger(Integer val1, Integer val2) {
        if (isNull(val1) && isNull(val2)) {
            return 0;
        } else if (isNull(val1)) {
            return 1;
        } else if (isNull(val2)) {
            return -1;
        } else {
            return val1.compareTo(val2);
        }
    }

    private int compareMoney(MoneyDO val1, MoneyDO val2) {
        if (isNull(val1) && isNull(val2)) {
            return 0;
        } else if (isNull(val1)) {
            return 1;
        } else if (isNull(val2)) {
            return -1;
        } else {
            return (int) (val1.getValue() - val2.getValue());
        }
    }

    private int compareDate(Date val1, Date val2) {
        if (isNull(val1) && isNull(val2)) {
            return 0;
        } else if (isNull(val1)) {
            return 1;
        } else if (isNull(val2)) {
            return -1;
        } else {
            return val1.compareTo(val2);
        }
    }

}
