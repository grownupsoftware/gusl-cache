package gusl.cache;

import gusl.core.errors.ErrorDO;
import gusl.core.exceptions.GUSLErrorException;

public enum CacheErrors {
    ERR_SERVER_ERROR("CACHE001 Internal error: {0}", "internal.error"),
    FAILED_TO_GET_GETTER_METHOD("CACHE002 Failed to get getter method {0}", "failed.to.get.getter.method"),
    TYPE_NOT_SUPPORTED("CACHE002 Error: {0}", "return.type.not.supported");

    private final String message;
    private final String messageKey;

    CacheErrors(String message, String messageKey) {
        this.message = message;
        this.messageKey = messageKey;
    }

    public ErrorDO getError() {
        return new ErrorDO(message, messageKey);
    }

    public ErrorDO getError(Long id) {
        if (id != null) {
            return new ErrorDO(null, message, messageKey, String.valueOf(id));
        } else {
            return new ErrorDO(message, messageKey);
        }
    }

    public ErrorDO getError(String... params) {
        if (params != null) {
            return new ErrorDO(null, message, messageKey, params);
        } else {
            return new ErrorDO(message, messageKey);
        }
    }

    public GUSLErrorException generateException(String params) {
        return new GUSLErrorException(getError(params));
    }

    public GUSLErrorException generateException(Throwable t, String... params) {
        return new GUSLErrorException(getError(params), t);
    }

    public GUSLErrorException generateExceptionNoLogging(String param) {
        final ErrorDO error = getError(param);
        error.setIgnoreLogging(true);
        return new GUSLErrorException(getError(param));
    }

    public GUSLErrorException generateExceptionNoLogging(String... params) {
        final ErrorDO error = getError(params);
        error.setIgnoreLogging(true);
        return new GUSLErrorException(error);
    }

}
