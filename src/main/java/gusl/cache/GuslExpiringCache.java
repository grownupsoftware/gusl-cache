package gusl.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.executors.BackgroundThreadPoolExecutor;
import gusl.core.logging.GUSLLogManager;
import gusl.core.logging.GUSLLogger;
import gusl.model.IdentifiableKey;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;

import static com.google.common.cache.RemovalCause.EXPIRED;
import static gusl.core.utils.Utils.safeStream;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

public abstract class GuslExpiringCache<K, V extends IdentifiableKey<K>> extends AbstractKeyDOCache<K, V> implements RemovalListener<K, V> {

    protected final GUSLLogger logger = GUSLLogManager.getLogger(this.getClass());

    private final Cache<K, V> theCache;
    private final String theCacheName;

    private static final Map<String, ScheduledFuture<?>> theFutureMap = new ConcurrentHashMap<>();
    private final int theCleanUpTime;
    private final TimeUnit theCleanUpTimeUnit;
    private long theLastUpdateTime;

    public GuslExpiringCache(String cacheName, int ttlTime, TimeUnit ttlTimeUnit, int cleanUpTime, TimeUnit cleanUpTimeUnit) {
        if (ttlTime > 0) {
            theCache = CacheBuilder.newBuilder().expireAfterAccess(ttlTime, ttlTimeUnit).removalListener(this).build();
        } else {
            theCache = CacheBuilder.newBuilder().removalListener(this).build();
        }
        theCacheName = cacheName;
        theCleanUpTime = cleanUpTime;
        theCleanUpTimeUnit = cleanUpTimeUnit;
        scheduleCleanUp();
    }

    protected abstract V fetch(K id) throws GUSLErrorException;

    public abstract void entryExpired(K key, V value);

    private void scheduleCleanUp() {
        if (theCleanUpTime != 0) {
            // only add scheduled clean up if there is a 'time'
            ScheduledFuture<?> shedulesFuture = BackgroundThreadPoolExecutor.scheduleAtFixedRate(() -> {
                logger.debug("Clear cache [{}] - start", theCacheName);
                theCache.cleanUp();
                logger.debug("Clear cache [{}] - end", theCacheName);
            }, 0, theCleanUpTime, theCleanUpTimeUnit);

            theFutureMap.put(theCacheName, shedulesFuture);
        }
    }

    /**
     * Cancels the clean up task
     */
    public void close() {
        ScheduledFuture<?> future = theFutureMap.get(theCacheName);
        if (future != null) {
            if (!future.isDone()) {
                future.cancel(true);
            }
        }

        theFutureMap.remove(theCacheName);
    }

    /**
     * Shuts down ALL clean up futures
     */
    public static void shutdown() {
        theFutureMap.entrySet().stream().forEach(entry -> {
            ScheduledFuture<?> future = entry.getValue();
            if (future != null) {
                if (!future.isDone()) {
                    future.cancel(true);
                }
            }
        });
    }

    @Override
    public void onRemoval(RemovalNotification<K, V> notification) {
        logger.debug("Removing from cache. Cause: {} Value: {}", notification.getCause(), notification.getValue());
        if (notification.getCause() == EXPIRED) {
            entryExpired(notification.getKey(), notification.getValue());
        }
    }

    @Override
    public void populateCache(List<? extends V> list) {
        safeStream(list).forEach(this::add);
        setPopulated();
    }

    public void add(V value) {
        if (nonNull(value)) {
            theCache.put(value.getKey(), value);
            theLastUpdateTime = System.currentTimeMillis();
        }
    }

    public void update(V data) {
        add(data);
    }

    public boolean remove(V data) {
        return removeKey(data.getKey());
    }

    public boolean removeKey(K id) {
        theCache.invalidate(id);
        return true;
    }

    public V get(K id) {
        // logger.info("theCache: {} id: {}", theCache, id);
        if (isNull(id)) {
            return null;
        }
        final V entity = theCache.getIfPresent(id);
        try {
            if (nonNull(entity)) {
                return entity;
            }
            final V newEntity = fetch(id);
            add(newEntity);
            return newEntity;
        } catch (GUSLErrorException e) {
            return null;
        }
    }

    public V getNoFetch(K id) {
        // logger.info("theCache: {} id: {}", theCache, id);
        if (isNull(id)) {
            return null;
        }
        return theCache.getIfPresent(id);
    }

    @Override
    public Optional<V> getOpt(K id) {
        return Optional.ofNullable(theCache.getIfPresent(id));
    }

    public void invalidate(K key) {
        theCache.invalidate(key);
    }

//    public void invalidateAll() {
//        theCache.invalidateAll();
//    }

    public boolean isEmpty() {
        return theCache.size() == 0;
    }

    @Override
    public List<V> getAll() {
        return new ArrayList<>(theCache.asMap().values());
    }

    public long getLastUpdateTime() {
        return theLastUpdateTime;
    }

    @Override
    public void setLastUpdateTime() {
        theLastUpdateTime = new Date().getTime();
    }

    @Override
    public List<V> getWithPredicate(Predicate<V> predicate) {
        return safeStream(theCache.asMap().values()).filter(predicate).collect(toList());
    }

    public String getCacheName() {
        return theCacheName;
    }

    @Override
    public void clear() {
        theCache.invalidateAll();
        theCache.cleanUp();
    }

    @Override
    public long getCacheSize() {
        return theCache.size();
    }

}
