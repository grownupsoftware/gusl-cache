package gusl.cache;

import gusl.core.eventbus.OnEvent;
import gusl.core.json.JsonUtils;
import gusl.core.utils.DateFormats;
import gusl.core.utils.Platform;
import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheAction;
import gusl.model.event.DumpCacheEvent;
import gusl.model.event.LogCacheEvent;
import lombok.CustomLog;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;

/**
 * Base Class for all DO Caches.
 * <p>
 * This will only update if the setPopulated, or listen has been called.
 * </p>
 *
 * @param <DO>
 * @author dhudson
 */
@CustomLog
public abstract class AbstractIdDOCache<DO> extends TimestampableImpl implements IdDoCache<DO> {

    private boolean isPopulated = false;

    private final Predicate<DO> theAllPredicate = t -> true;

    public AbstractIdDOCache() {
    }

    public Predicate<DO> getAllPredicate() {
        return theAllPredicate;
    }

    @Override
    public List<DO> getAll() {
        return getWithPredicate(getAllPredicate());
    }

    /**
     * Listen for new events.
     */
    @Override
    public void listen() {
        setPopulated();
    }

    public void setPopulated() {
        isPopulated = true;
    }

    public boolean isPopulated() {
        return isPopulated;
    }

    public void updateCache(AbstractCacheActionEvent<? extends DO> event) {
        if (isPopulated) {
            updateCache(event.getDataObject(), event.getCacheAction());
        }
    }

    public void updateCache(DO data, CacheAction cacheAction) {
        if (data != null) {
            switch (cacheAction) {
                case ADD:
                    add(data);
                    break;
                case UPDATE:
                    update(data);
                    break;
                case DELETE:
                case REMOVE:
                    remove(data);
                    break;
                case DUMP:
                    dump();
                case CACHE_SIZE:
                    getCacheSize();
                    break;
                case CLEAR:
                    clear();
                    break;
            }
        }
    }

    @OnEvent
    public void handleEvent(DumpCacheEvent event) {
        dump();
    }

    @OnEvent
    public void handleEvent(LogCacheEvent event) {
        logCacheSize();
    }

    public void logCacheSize() {
        logger.info("Cache [{}] No. Records [{}]", this.getClass().getSimpleName(), getCacheSize());
    }

    public void dump() {
        try {
            File dump = new File(Platform.getTmpDirectory()
                    + File.separator
//                    + theNodeDetails.getNodeType().name()
//                    + "_"
//                    + theNodeDetails.getNodeId()
//                    + "_"
                    + this.getClass().getSimpleName()
                    + "_"
                    + DateFormats.formatDate("yyyyMMdd_HHmmss", new Date())
                    + ".json");

            JsonUtils.storeObject(getAll(), dump.getAbsolutePath());
        } catch (IOException ex) {
            logger.warn("Failed to dump cache for {}", this.getClass().getSimpleName(), ex);
        }
    }

    @OnEvent
    public void handleDumpEvent(DumpCacheEvent event) {
        dump();
    }

    public abstract void add(DO data);

    public abstract void update(DO data);

    public abstract boolean remove(DO data);

    public abstract void clear();

}
