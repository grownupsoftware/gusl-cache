package gusl.cache;

import gusl.core.exceptions.GUSLErrorException;
import gusl.model.dao.DAOInterface;

/**
 * @author dhudson
 * @since 11/02/2022
 */
public interface CacheDAOInterface<T> extends DAOInterface<T> {

    T insertAndSend(T entity) throws GUSLErrorException;

    T updateAndSend(T entity) throws GUSLErrorException;

    void deleteAndSend(T entity) throws GUSLErrorException;
}
