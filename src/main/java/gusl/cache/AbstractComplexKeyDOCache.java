package gusl.cache;

import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheAction;

import java.util.List;
import java.util.function.Predicate;

/**
 * Base Class for all DO Caches.
 * <p>
 * This will only update if the setPopulated has been called.
 *
 * @param <DO>
 * @author dhudson
 */
public abstract class AbstractComplexKeyDOCache<KEY, DO> extends TimestampableImpl implements ComplexKeyDoCache<KEY, DO> {

    private boolean isPopulated = false;

    private final Predicate<DO> theAllPredicate = new Predicate<DO>() {
        @Override
        public boolean test(Object t) {
            return true;
        }
    };

    public AbstractComplexKeyDOCache() {
    }

    public Predicate<DO> getAllPredicate() {
        return theAllPredicate;
    }

    @Override
    public List<DO> getAll() {
        return getWithPredicate(getAllPredicate());
    }

    public void setPopulated() {
        isPopulated = true;
    }

    public boolean isPopulated() {
        return isPopulated;
    }

    public void updateCache(AbstractCacheActionEvent<? extends DO> event) {
        if (isPopulated) {
            if (isPopulated) {
                updateCache(event.getDataObject(), event.getCacheAction());
            }
        }
    }

    public void updateCache(DO data, CacheAction cacheAction) {
        if (isPopulated) {
            switch (cacheAction) {
                case ADD:
                    add(data);
                    break;
                case UPDATE:
                    update(data);
                    break;
                case REMOVE:
                    remove(data);
                    break;
                case CACHE_SIZE:
                    getCacheSize();
                    break;
                case CLEAR:
                    clear();
                    break;
            }
        }
    }

    @Override
    public void listen() {
        setPopulated();
    }

    public abstract void add(DO data);

    public abstract void update(DO data);

    public abstract boolean remove(DO data);

    public abstract void clear();

}
