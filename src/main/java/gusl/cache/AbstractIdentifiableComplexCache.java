package gusl.cache;

import gusl.core.utils.Utils;
import gusl.model.Identifiable;
import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheAction;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

public abstract class AbstractIdentifiableComplexCache<KEY, DO extends Identifiable<KEY>> extends AbstractComplexKeyDOCache<KEY, DO> {

    private final Map<KEY, DO> theMap = new ConcurrentHashMap<>();

    public AbstractIdentifiableComplexCache() {
    }

    public Optional<DO> getSingleWithPredicate(Predicate<DO> predicate) {
        if (predicate == null) {
            return Optional.empty();
        }

        return Utils.safeStream(theMap.values()).filter(predicate).findFirst();
    }

    @Override
    public List<DO> getWithPredicate(Predicate<DO> predicate) {
        if (predicate == null) {
            predicate = getAllPredicate();
        }

        return Utils.safeStream(theMap.values())
                .filter(predicate)
                .collect(Collectors.toList());
    }

    @Override
    public void populateCache(List<? extends DO> contents) {
        for (DO content : contents) {
            add(content);
        }
        setPopulated();
    }

    @Override
    public List<DO> getAll() {
        return new ArrayList<>(theMap.values());
    }

    @Override
    public DO get(KEY id) {
        return theMap.get(id);
    }

    @Override
    public DO getNoFetch(KEY id) {
        return theMap.get(id);
    }

    @Override
    public void add(DO data) {
        theMap.put(data.getId(), data);
        setLastUpdateTime();
    }

    @Override
    public void update(DO data) {
        add(data);
    }

    @Override
    public boolean remove(DO data) {
        return removeByKey(data.getId());
    }

    @Override
    public void clear() {
        theMap.clear();
        setLastUpdateTime();
    }

    public boolean removeByKey(KEY id) {

        boolean didRemove = (theMap.remove(id) != null);

        if (didRemove) {
            setLastUpdateTime();
        }
        return didRemove;
    }

    public boolean contains(KEY id) {
        return theMap.containsKey(id);
    }

    public Collection<DO> values() {
        return theMap.values();
    }

    @Override
    public Optional<DO> getOpt(KEY id) {
        return Optional.ofNullable(get(id));
    }

    @Override
    public long getCacheSize() {
        return theMap.values().size();
    }

    public <T> boolean hasEvent(AbstractCacheActionEvent<T> event) {
        return nonNull(event) && nonNull(event.getCacheAction()) && (nonNull(event.getDataObject()) || CacheAction.CLEAR == event.getCacheAction());
    }

}
