package gusl.cache;

import com.google.common.base.CaseFormat;
import gusl.annotations.form.page.OrderDirection;
import gusl.annotations.form.page.PageRequest;
import gusl.core.exceptions.GUSLErrorException;
import gusl.model.money.MoneyDO;
import lombok.CustomLog;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.Date;

import static java.util.Objects.isNull;

@CustomLog
public class ComparatorHelper {

    public static <T> Comparator<T> getComparator(PageRequest request, Class<T> doClass) throws GUSLErrorException {
        if (isNull(request) || isNull(request.getOrderParam())) {
            // return a non sort comparator
            // logger.info("page request: request: {} return no sort", request);
            return (o1, o2) -> 0;
        }
        logger.debug("comparing will use : {} {}  on {}", request.getOrderParam(), request.getOrderDirection(), doClass.getCanonicalName());
        return getComparator(request.getOrderParam(), request.getOrderDirection(), doClass);
    }

    public static <T> Comparator<T> getComparator(String field, OrderDirection direction, Class<T> doClass) throws GUSLErrorException {
        if (isNull(field)) {
            // return a non sort comparator
            // logger.info("page request: field is null");
            return (o1, o2) -> 0;
        }
        Comparator<T> comparator = comparator = createComparator(field, doClass);
        switch (direction) {
            case DESC:
                return comparator.reversed();
            case ASC:
            default:
                return comparator;
        }
    }

    private static String normaliseName(String name) {
        if (name.contains("-")) {
            return CaseFormat.LOWER_HYPHEN.to(CaseFormat.UPPER_CAMEL, name);
        } else if (name.contains("_")) {
            return CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, name);
        }
        return CaseFormat.LOWER_CAMEL.to(CaseFormat.UPPER_CAMEL, name);
    }

    private static String getMethodName(String prefix, String name) {
        return prefix + name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    private static <T> Comparator<T> createComparator(String field, Class<T> doClass) throws GUSLErrorException {
        String fieldName = normaliseName(field);
        Method getterMethod;
        try {
            getterMethod = doClass.getMethod(getMethodName("get", fieldName));
        } catch (NoSuchMethodException ex) {
            throw new GUSLErrorException(CacheErrors.ERR_SERVER_ERROR.getError(doClass.getCanonicalName(), fieldName));
        }
        return createComparator(getterMethod);
    }

    private static <T> Comparator<T> createComparator(Method getterMethod) {
        return (o1, o2) -> {
            // logger.info("comparator on: method: {} type: {}",getterMethod.getName(),getterMethod.getReturnType().getSimpleName());

            try {
                if (getterMethod.getReturnType().isEnum()) {
                    return compareEnums((Enum) getterMethod.invoke(o1), (Enum) getterMethod.invoke(o2));
                } else {

                    switch (getterMethod.getReturnType().getSimpleName()) {
                        case "String":
                            return compareStrings((String) getterMethod.invoke(o1), (String) getterMethod.invoke(o2));
                        case "Long":
                        case "long":
                            return compareLong((Long) getterMethod.invoke(o1), (Long) getterMethod.invoke(o2));
                        case "Integer":
                        case "int":
                            return compareInteger((Integer) getterMethod.invoke(o1), (Integer) getterMethod.invoke(o2));
                        case "Money":
                            return compareMoney((MoneyDO) getterMethod.invoke(o1), (MoneyDO) getterMethod.invoke(o2));
                        case "Date":
                            return compareDate((Date) getterMethod.invoke(o1), (Date) getterMethod.invoke(o2));
                        case "LocalDate":
                            return compareLocalDate((LocalDate) getterMethod.invoke(o1), (LocalDate) getterMethod.invoke(o2));
                        case "LocalDateTime":
                            return compareLocalDateTime((LocalDateTime) getterMethod.invoke(o1), (LocalDateTime) getterMethod.invoke(o2));
                        case "ZonedDateTime":
                            return compareZonedDateTime((ZonedDateTime) getterMethod.invoke(o1), (ZonedDateTime) getterMethod.invoke(o2));
                    }
                }

                return 0;
            } catch (IllegalAccessException | InvocationTargetException e) {
                logger.warn("Failed to apply predicated", e);
                return 0;
            }
        };
    }

    private static int compareLocalDate(LocalDate val1, LocalDate val2) {
        if (isNull(val1) && isNull(val2)) {
            return 0;
        } else if (isNull(val1)) {
            return 1;
        } else if (isNull(val2)) {
            return -1;
        } else {
            return val1.compareTo(val2);
        }
    }

    private static int compareLocalDateTime(LocalDateTime val1, LocalDateTime val2) {
        if (isNull(val1) && isNull(val2)) {
            return 0;
        } else if (isNull(val1)) {
            return 1;
        } else if (isNull(val2)) {
            return -1;
        } else {
            return val1.compareTo(val2);
        }
    }

    private static int compareZonedDateTime(ZonedDateTime val1, ZonedDateTime val2) {
        if (isNull(val1) && isNull(val2)) {
            return 0;
        } else if (isNull(val1)) {
            return 1;
        } else if (isNull(val2)) {
            return -1;
        } else {
            return val1.compareTo(val2);
        }
    }

    private static int compareEnums(Enum val1, Enum val2) {
        if (isNull(val1) && isNull(val2)) {
            return 0;
        } else if (isNull(val1)) {
            return 1;
        } else if (isNull(val2)) {
            return -1;
        } else {
            return val1.compareTo(val2);
        }
    }

    private static int compareStrings(String val1, String val2) {
        if (isNull(val1) && isNull(val2)) {
            return 0;
        } else if (isNull(val1)) {
            return 1;
        } else if (isNull(val2)) {
            return -1;
        } else {
            return val1.compareTo(val2);
        }
    }

    private static int compareLong(Long val1, Long val2) {
        if (isNull(val1) && isNull(val2)) {
            return 0;
        } else if (isNull(val1)) {
            return 1;
        } else if (isNull(val2)) {
            return -1;
        } else {
            return val1.compareTo(val2);
        }
    }

    private static int compareInteger(Integer val1, Integer val2) {
        if (isNull(val1) && isNull(val2)) {
            return 0;
        } else if (isNull(val1)) {
            return 1;
        } else if (isNull(val2)) {
            return -1;
        } else {
            return val1.compareTo(val2);
        }
    }

    private static int compareMoney(MoneyDO val1, MoneyDO val2) {
        if (isNull(val1) && isNull(val2)) {
            return 0;
        } else if (isNull(val1)) {
            return 1;
        } else if (isNull(val2)) {
            return -1;
        } else {
            return (int) (val1.getValue() - val2.getValue());
        }
    }

    private static int compareDate(Date val1, Date val2) {
        if (isNull(val1) && isNull(val2)) {
            return 0;
        } else if (isNull(val1)) {
            return 1;
        } else if (isNull(val2)) {
            return -1;
        } else {
            return val1.compareTo(val2);
        }
    }
}
