package gusl.cache;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * @param <V>
 * @author dhudson
 */
public interface KeyDoCache<K, V> extends Timestampable {

    List<V> getAll();

    List<V> getWithPredicate(Predicate<V> predicate);

    void populateCache(List<? extends V> contents);

    V get(K id);

    V getNoFetch(K id);

    Optional<V> getOpt(K id);

    void listen();

    long getCacheSize();

    boolean isPopulated();

    boolean remove(V data);
}
