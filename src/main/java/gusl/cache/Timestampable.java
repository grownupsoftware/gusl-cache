package gusl.cache;

/**
 * @author dhudson
 */
public interface Timestampable {

    long getLastUpdateTime();

    void setLastUpdateTime();
}
