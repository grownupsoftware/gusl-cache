package gusl.cache;

/**
 * Simple Timestamp class
 *
 * @author dhudson
 */
public class TimestampableImpl implements Timestampable {

    private long theLastUpdated;

    @Override
    public long getLastUpdateTime() {
        return theLastUpdated;
    }

    @Override
    public void setLastUpdateTime() {
        theLastUpdated = System.currentTimeMillis();
    }
}
