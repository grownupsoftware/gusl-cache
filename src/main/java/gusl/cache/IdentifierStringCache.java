package gusl.cache;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * @author dhudson
 * @since 02/09/2022
 */
public interface IdentifierStringCache<DO> {

    public Optional<DO> getSingleWithPredicate(Predicate<DO> predicate);

    public List<DO> getWithPredicate(Predicate<DO> predicate);

    public void populateCache(List<? extends DO> contents);

    public DO get(String id);

    public DO getNoFetch(String id);

    public void add(DO data);

    public void clear();

    public void update(DO data);

    boolean remove(DO data);

    boolean remove(String id);

    boolean contains(String id);

    Collection<DO> values();

    public Optional<DO> getOpt(String id);

    String getAdminUser();

    long getCacheSize();
}
