package gusl.cache;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public interface ComplexKeyDoCache<KEY, DO> extends Timestampable {

    List<DO> getAll();

    List<DO> getWithPredicate(Predicate<DO> predicate);

    void populateCache(List<? extends DO> contents);

    DO get(KEY id);

    DO getNoFetch(KEY id);

    Optional<DO> getOpt(KEY id);

    void listen();

    long getCacheSize();

}
