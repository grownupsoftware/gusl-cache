package gusl.cache;

import gusl.core.eventbus.OnEvent;
import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheAction;
import gusl.model.event.LogCacheEvent;
import lombok.CustomLog;

import javax.inject.Inject;
import java.util.List;
import java.util.function.Predicate;

/**
 * Base Class for all DO Caches.
 * <p>
 * This will only update if the setPopulated has been called.
 *
 * @param <DO>
 * @author dhudson
 */
@CustomLog
public abstract class AbstractKeyDOCache<K, DO> extends TimestampableImpl implements KeyDoCache<K, DO> {

    private final Predicate<DO> theAllPredicate = t -> true;
    private boolean isPopulated = false;

    AbstractKeyDOCache() {
    }

    Predicate<DO> getAllPredicate() {
        return theAllPredicate;
    }

    @Override
    public List<DO> getAll() {
        return getWithPredicate(getAllPredicate());
    }

    void setPopulated() {
        isPopulated = true;
    }

    public boolean isPopulated() {
        return isPopulated;
    }

    public void updateCache(final AbstractCacheActionEvent<? extends DO> event) {
        if (isPopulated) {
            updateCache(event.getDataObject(), event.getCacheAction());
        }
    }

    public void updateCache(final DO data, final CacheAction cacheAction) {
        if (isPopulated) {
            switch (cacheAction) {
                case ADD:
                    add(data);
                    break;
                case UPDATE:
                    update(data);
                    break;
                case DELETE:
                case REMOVE:
                    remove(data);
                    break;
                case CACHE_SIZE:
                    getCacheSize();
                    break;
                case CLEAR:
                    clear();
                    break;
            }
        }
    }

    @OnEvent
    public void handleEvent(final LogCacheEvent event) {
        logCacheSize();
    }

    private void logCacheSize() {
        logger.info("Cache [{}] No. Records [{}]", this.getClass().getSimpleName(), getCacheSize());
    }

    @Override
    public void listen() {
        setPopulated();
    }

    public abstract void add(DO data);

    public abstract void update(DO data);

    public abstract boolean remove(DO data);

    public abstract void clear();

}
